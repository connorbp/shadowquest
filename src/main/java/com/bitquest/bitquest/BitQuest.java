package com.bitquest.bitquest;

import com.bitquest.bitquest.commands.*;
import com.bitquest.bitquest.events.*;
import com.bitquest.bitquest.PasswordGenerator;
import com.google.gson.JsonObject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.DriverManager;
import java.text.ParseException;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import redis.clients.jedis.Jedis;

// Color Table :
// GREEN : Worked, YELLOW : Processing, LIGHT_PURPLE : Any money balance, BLUE : Player name,
// DARK_BLUE UNDERLINE : Link, RED : Server error, DARK_RED : User error, GRAY : Info, DARK_GRAY :
// Clan, DARK_GREEN : Landname

public class BitQuest extends JavaPlugin {
  // TODO: remove env variables not being used anymore
  // Connecting to REDIS
  // Links to the administration account via Environment Variables
  public static final String BITQUEST_ENV =
      System.getenv("BITQUEST_ENV") != null ? System.getenv("BITQUEST_ENV") : "development";
  public static final UUID ADMIN_UUID =
      System.getenv("ADMIN_UUID") != null ? UUID.fromString(System.getenv("ADMIN_UUID")) : null;

  public static final String BITCOIN_NODE_HOST =
      System.getenv("BITCOIN_PORT_8332_TCP_ADDR") != null
          ? System.getenv("BITCOIN_PORT_8332_TCP_ADDR")
          : null;
  public static final int BITCOIN_NODE_PORT =
      System.getenv("BITCOIN_PORT_8332_TCP_PORT") != null
          ? Integer.parseInt(System.getenv("BITCOIN_PORT_8332_TCP_PORT"))
          : 8332;
  public static final String SHADOW_NODE_HOST =
      System.getenv("SHADOW_PORT_9697_TCP_ADDR") != null
          ? System.getenv("BITCOIN_PORT_8332_TCP_ADDR")
          : "localhost";
  public static final int SHADOW_NODE_PORT =
      System.getenv("SHADOW_PORT_9697_TCP_PORT") != null
          ? Integer.parseInt(System.getenv("BITCOIN_PORT_8332_TCP_PORT"))
          : 8789;
  public static final String SERVERDISPLAY_NAME =
      System.getenv("SERVERDISPLAY_NAME") != null ? System.getenv("SERVERDISPLAY_NAME") : "Shadow";
  public static final Long DENOMINATION_FACTOR =
      System.getenv("DENOMINATION_FACTOR") != null
          ? Long.parseLong(System.getenv("DENOMINATION_FACTOR"))
          : 1L;
  public static final Long FEE_DENOMINATION_FACTOR =
      System.getenv("FEE_DENOMINATION_FACTOR") != null
          ? Long.parseLong(System.getenv("FEE_DENOMINATION_FACTOR"))
          : 100000000L;
  public static final String DENOMINATION_NAME =
      System.getenv("DENOMINATION_NAME") != null ? System.getenv("DENOMINATION_NAME") : "Crystals";
  public static final String BLOCKCYPHER_CHAIN =
      System.getenv("BLOCKCYPHER_CHAIN") != null ? System.getenv("BLOCKCYPHER_CHAIN") : "btc/test3";
  public static final String BITCOIN_NODE_USERNAME = System.getenv("BITCOIN_ENV_USERNAME");
  public static final String BITCOIN_NODE_PASSWORD = System.getenv("BITCOIN_ENV_PASSWORD");
  public static final String DISCORD_HOOK_URL = System.getenv("DISCORD_HOOK_URL");
  public static final String BLOCKCYPHER_API_KEY =
      System.getenv("BLOCKCYPHER_API_KEY") != null ? System.getenv("BLOCKCYPHER_API_KEY") : null;
  public static final Long MINER_FEE =
          System.getenv("MINER_FEE") != null ? Long.parseLong(System.getenv("MINER_FEE")) : 100000000;

  public static final int MAX_STOCK = 100;
  public static final String SERVER_NAME =
      System.getenv("SERVER_NAME") != null ? System.getenv("SERVER_NAME") : "ShadowQuest";

  // Can save world data in elasticsearch (optional)
  public static final String ELASTICSEARCH_ENDPOINT =
      System.getenv("ELASTICSEARCH_ENDPOINT") != null
          ? System.getenv("ELASTICSEARCH_ENDPOINT")
          : null;
  // REDIS: Look for Environment variables on hostname and port, otherwise defaults to
  // localhost:6379
  public static final String REDIS_HOST =
      System.getenv("REDIS_PORT_6379_TCP_ADDR") != null
          ? System.getenv("REDIS_PORT_6379_TCP_ADDR")
          : "localhost";
  public static final Integer REDIS_PORT =
      System.getenv("REDIS_PORT_6379_TCP_PORT") != null
          ? Integer.parseInt(System.getenv("REDIS_PORT_6379_TCP_PORT"))
          : 6379;
  public static final Jedis REDIS = new Jedis(REDIS_HOST, REDIS_PORT);


  public static final String CURRENCY_ID =
      System.getenv("CURRENCY_ID") != null ? System.getenv("CURRENCY_ID") : "8612701917745363683";//DEVCOIN: 10126500136774276517 //denomination factor for devcoin: 6 zeros

  // Default price: 10,000 satoshis or 100 bits
  public static final Long LAND_PRICE =
      System.getenv("LAND_PRICE") != null ? Long.parseLong(System.getenv("LAND_PRICE")) : 500;
  // How much to add to land price near spawn
  public static final Double LAND_PRICE_SPAWN_TAX =
  System.getenv("LAND_PRICE_SPAWN_TAX") != null ? Double.parseDouble(System.getenv("LAND_PRICE_SPAWN_TAX")) : 400;
  //lower speed factor makes the prices get cheaper faster
  public static final Double LAND_TAX_DISTANCE =
  System.getenv("LAND_TAX_DISTANCE") != null ? Double.parseDouble(System.getenv("LAND_TAX_DISTANCE")) : 250;//distance in chunks until land tax drops off
  // Minimum transaction by default is 100 gems
  public static final Long MINIMUM_TRANSACTION =
      System.getenv("MINIMUM_TRANSACTION") != null
          ? Long.parseLong(System.getenv("MINIMUM_TRANSACTION"))
          : 10L;

  public static final Integer DO_FUEL_REFIL =
      System.getenv("DO_FUEL_REFIL") != null
          ? Integer.parseInt(System.getenv("DO_FUEL_REFIL"))
          : 1;

  public static final Long FUEL_REFIL_UNDER =
      System.getenv("FUEL_REFIL_UNDER") != null
          ? Long.parseLong(System.getenv("FUEL_REFIL_UNDER"))
          : 10L;
  public static final Long FUEL_REFIL_AMMOUNT =
      System.getenv("FUEL_REFIL_AMMOUNT") != null
          ? Long.parseLong(System.getenv("FUEL_REFIL_AMMOUNT"))
          : 100L;
  

          //FUEL_REFIL
          //FUEL_REFIL_UNDER
          //FUEL_REFIL_AMMOUNT

  public static int rand(int min, int max) {
    return min + (int) (Math.random() * ((max - min) + 1));
  }

  public Wallet wallet = null;
  public Player last_loot_player;
  public boolean spookyMode = false;
  // caches is used to reduce the amounts of calls to redis, storing some chunk information in
  // memory
  public HashMap<String, Boolean> land_unclaimed_cache = new HashMap();
  public HashMap<String, String> land_owner_cache = new HashMap();
  public static HashMap<String, String> land_permission_cache = new HashMap();
  public HashMap<String, String> land_name_cache = new HashMap();
  public Long wallet_balance_cache = 0L;
  public ArrayList<ItemStack> books = new ArrayList<ItemStack>();
  // when true, server is closed for maintenance and not allowing players to join in.
  public boolean maintenance_mode = false;
  private Map<String, CommandAction> commands;
  private Map<String, CommandAction> modCommands;
  private Player[] moderators;
  public static long PET_PRICE = 100 * DENOMINATION_FACTOR;

  public static final String db_url =
      "jdbc:postgresql://"
          + System.getenv("POSTGRES_PORT_5432_TCP_ADDR")
          + ":"
          + System.getenv("POSTGRES_PORT_5432_TCP_PORT")
          + "/bitquest";
  public java.sql.Connection db_con;

  @Override
  public void onEnable() {
    log("[startup] ShadowQuest starting");

    System.out.println("Checking that POSTGRES_PORT_5432_TCP_PORT envoronment variable exists...");
    if (System.getenv("POSTGRES_PORT_5432_TCP_PORT") == null) {
      Bukkit.shutdown();
      System.out.println("Please set the POSTGRES_PORT_5432_TCP_PORT environment variable");
    }
    ;

    try {
      Class.forName("org.postgresql.Driver");
      this.db_con =
          DriverManager.getConnection(
              this.db_url,
              System.getenv("POSTGRES_ENV_POSTGRES_USER"),
              System.getenv("POSTGRES_ENV_POSTGRES_PASSWORD"));
      DBMigrationCheck migration = new DBMigrationCheck(this.db_con);

      if (ADMIN_UUID == null) {
        log("[warning] ADMIN_UUID env variable to is not set.");
      }

      // registers listener classes
      getServer().getPluginManager().registerEvents(new ChatEvents(this), this);
      getServer().getPluginManager().registerEvents(new BlockEvents(this), this);
      getServer().getPluginManager().registerEvents(new EntityEvents(this), this);
      getServer().getPluginManager().registerEvents(new InventoryEvents(this), this);
      getServer().getPluginManager().registerEvents(new SignEvents(this), this);
      getServer().getPluginManager().registerEvents(new ServerEvents(this), this);

      // player does not lose inventory on death
      System.out.println("[startup] sending command gamerule keepInventory on");

      Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule keepInventory on");

      // loads config file. If it doesn't exist, creates it.
      getDataFolder().mkdir();
      System.out.println("[startup] checking default config file");

      if (!new java.io.File(getDataFolder(), "config.yml").exists()) {
        saveDefaultConfig();
        System.out.println("[startup] config file does not exist. creating default.");
      }

      // loads world wallet from env variables. If not present, generates a new one each time the
      // server is run.
      if (System.getenv("SG_PRIVATE") != null
          && System.getenv("SG_PUBLIC") != null
          && System.getenv("SG_ADDRESS") != null
          && System.getenv("SG_ACCOUNT") != null) {
        wallet =
                new Wallet(
                        System.getenv("SG_PRIVATE"),
                        System.getenv("SG_PUBLIC"),
                        System.getenv("SG_ADDRESS"),
                        System.getenv("SG_ACCOUNT"));
        System.out.println("[world wallet] imported from environment");
      } else if (REDIS.exists("private")&&REDIS.exists("public")&&REDIS.exists("address")&&REDIS.exists("account")){
        wallet =
                new Wallet(
                        REDIS.get("private"),
                        REDIS.get("public"),
                        REDIS.get("address"),
                        REDIS.get("account"));
      } else {
        try{
          wallet = this.generateNewWallet("worldwallet");
          
          System.out.println("[world wallet] generated new wallet");
          REDIS.set("address", wallet.getAddress());
          REDIS.set("private", wallet.getPrivateKey());
          REDIS.set("public", wallet.getPublicKey());
          REDIS.set("account", wallet.getAccount());

        } catch(Exception e) {
          System.out.println("[world wallet] Failed To Generate world wallet exception:\n" + e.toString());
        }
      }
      System.out.println("[world wallet] address: " + wallet.getAddress());

      if (BITCOIN_NODE_HOST != null) {
        System.out.println("[startup] checking bitcoin node connection");
        getBlockChainInfo();
      }

      // creates scheduled timers (update balances, etc)
      createScheduledTimers();
      sendDiscordMessage("ShadowQuest starting");
      commands = new HashMap<String, CommandAction>();
      commands.put("wallet", new WalletCommand(this));
      commands.put("land", new LandCommand(this));
      commands.put("home", new HomeCommand(this));
      commands.put("clan", new ClanCommand(this));
      commands.put("transfer", new TransferCommand(this));
      commands.put("report", new ReportCommand(this));
      commands.put("send", new SendCommand(this));
      commands.put("currency", new CurrencyCommand(this));
      commands.put("donate", new DonateCommand(this));
      commands.put("profession", new ProfessionCommand(this));
      commands.put("spawn", new SpawnCommand(this));
      commands.put("pet", new PetCommand(this));
      modCommands = new HashMap<String, CommandAction>();
      modCommands.put("butcher", new ButcherCommand());
      modCommands.put("killAllVillagers", new KillAllVillagersCommand(this));
      modCommands.put("crashTest", new CrashtestCommand(this));
      modCommands.put("mod", new ModCommand());
      modCommands.put("ban", new BanCommand());
      modCommands.put("unban", new UnbanCommand());
      modCommands.put("banlist", new BanlistCommand());
      modCommands.put("spectate", new SpectateCommand(this));
      modCommands.put("emergencystop", new EmergencystopCommand());
      modCommands.put("fixabandonland", new FixAbandonLand());
      modCommands.put("motd", new MOTDCommand(this));
      // TODO: Remove this command after migrate.
      modCommands.put("migrateclans", new MigrateClansCommand());
      System.out.println("[startup] finished");
      publish_stats();
      REDIS.set("loot:rate:limit", "1");
      REDIS.expire("loot:rate:limit", 10);
      killAllVillagers();
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("[fatal] plugin enable fails");
      Bukkit.shutdown();
    }
  }
  public void createBossFight(Location location) {
    if(location.getWorld().getName().equals("world_the_end")) {
      World w=Bukkit.getWorld("world_the_end");
      List<Entity> entities = w.getEntities();
      boolean already_spawned=false;
      for (Entity en : entities) {
        if ((en instanceof Giant)) {
          already_spawned=true;
        }
      }

      if(already_spawned==false) {
        location.getWorld().spawnEntity(location,EntityType.GIANT);
      } else {
        System.out.println("[boss fight] a giant already spawned");
      }
    }

  }



  public static final Wallet generateNewWallet(String UserNameOrSeed)
      throws IOException, org.json.simple.parser.ParseException {

    //http://localhost:8789/nxt?=/nxt&requestType=getAccountId&secretPhrase=secretphrasehere

    JSONParser parser = new JSONParser();

    PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
        .useDigits(false)
        .useLower(true)
        .useUpper(true)
        .usePunctuation(false)
        .build();
    String password = passwordGenerator.generate(40, UserNameOrSeed);

      URL url = new URL("http://" + BitQuest.SHADOW_NODE_HOST + ":" + BitQuest.SHADOW_NODE_PORT + "/nxt?requestType=getAccountId&secretPhrase=" + password);
      
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setConnectTimeout(5000);

      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
      con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
      con.setRequestProperty( "charset", "utf-8");
      con.setRequestProperty( "Content-Length", Integer.toString( 0 ));
      
      int responseCode = con.getResponseCode();

      System.out.println("[generatewallet] Received Response Code: " + responseCode);

      if (responseCode == HttpURLConnection.HTTP_OK) { //success
			    BufferedReader in = new BufferedReader(new InputStreamReader(
				    	con.getInputStream()));
			    String inputLine;
			    StringBuffer response = new StringBuffer();

			    while ((inputLine = in.readLine()) != null) {
			    	response.append(inputLine);
			    }
			    in.close();
          System.out.println("[generatewallet] Received Response Data:\n" + response.toString());

          JSONObject response_object = (JSONObject) parser.parse(response.toString());

          String accountAddress = response_object.get("accountRS").toString();
          String accountPublicKey = response_object.get("publicKey").toString();
          String accountID = response_object.get("account").toString();

          if(accountAddress == null || accountAddress.length() == 0
          || accountPublicKey == null || accountPublicKey.length() == 0
          || accountID == null || accountID.length() == 0) {
            throw new IOException("Bad Response Data.");
          }

          System.out.println("\nCreated wallet: :\n"
          + "Address: " + accountAddress + "\n"
          + "publicKey: " + accountPublicKey + "\n"
          + "ID: " + accountID + "\n");

        return new Wallet(
            password,
            accountPublicKey,
            accountAddress,
            accountID);

      } else {
		    	throw new IOException("Bad URL Response Code: " + responseCode);
		  }
  }
  // @todo: make this just accept the endpoint name and (optional) parameters
  public JSONObject getBlockChainInfo() throws org.json.simple.parser.ParseException {
    JSONParser parser = new JSONParser();

    try {
      final JSONObject jsonObject = new JSONObject();
      jsonObject.put("jsonrpc", "1.0");
      jsonObject.put("id", "bitquest");
      jsonObject.put("method", "getblockchaininfo");
      JSONArray params = new JSONArray();
      jsonObject.put("params", params);
      System.out.println("Checking blockchain info...");
      URL url = new URL("http://" + BITCOIN_NODE_HOST + ":" + BITCOIN_NODE_PORT);
      System.out.println(url.toString());
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      String userPassword = BITCOIN_NODE_USERNAME + ":" + BITCOIN_NODE_PASSWORD;
      String encoding = java.util.Base64.getEncoder().encodeToString(userPassword.getBytes());
      con.setRequestProperty("Authorization", "Basic " + encoding);

      con.setRequestMethod("POST");
      con.setRequestProperty("User-Agent", "bitquest plugin");
      con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
      con.setDoOutput(true);
      OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
      out.write(jsonObject.toString());
      out.close();

      int responseCode = con.getResponseCode();

      BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();
      System.out.println(response.toString());
      return (JSONObject) parser.parse(response.toString());
    } catch (IOException e) {
      System.out.println("problem connecting with bitcoin node");
      System.out.println(e);
      // Unable to call API?
    }

    return new JSONObject(); // just give them an empty object
  }

  public void announce(final String message) {
    for (Player player : Bukkit.getOnlinePlayers()) {
      player.sendMessage(message);
    }
  }

  public boolean checkFuel(Wallet userWallet) {
    
    try{

      Long fuelBalance = userWallet.getFuelBalance(false);
      Long currentTimeStamp = System.currentTimeMillis();
      if(DO_FUEL_REFIL == 1) {
          if(fuelBalance <= FUEL_REFIL_UNDER * FEE_DENOMINATION_FACTOR && currentTimeStamp - userWallet.lastRefilTime > 60000) {
            userWallet.lastRefilTime = currentTimeStamp;
            System.out.println("[checkfuel] Refilling wallet fuel for account " + userWallet.getAddress());
            wallet.sendFuel(userWallet.getAddress(), userWallet.getPublicKey(), FUEL_REFIL_AMMOUNT * FEE_DENOMINATION_FACTOR);
          }
      }

      return userWallet.checkFuelCanSend();

    } catch (Exception e) {
       System.out.println("[checkfuel] exception checking fuel:\n" + e.toString());
       return false;
    }
  }

  public void updateScoreboard(final Player player) {
    try {
      final User user = new User(this.db_con, player.getUniqueId());
      ScoreboardManager scoreboardManager;
      Scoreboard walletScoreboard;
      Objective walletScoreboardObjective;
      scoreboardManager = Bukkit.getScoreboardManager();
      walletScoreboard = scoreboardManager.getNewScoreboard();
      walletScoreboardObjective = walletScoreboard.registerNewObjective("wallet", "dummy");

      walletScoreboardObjective.setDisplaySlot(DisplaySlot.SIDEBAR);

      walletScoreboardObjective.setDisplayName(
          ChatColor.DARK_PURPLE
              + ChatColor.BOLD.toString()
              + BitQuest.SERVERDISPLAY_NAME
              + ChatColor.GRAY
              + ChatColor.BOLD.toString()
              + "Quest");

      if (BitQuest.BLOCKCYPHER_CHAIN != null) {
        Score score =
            walletScoreboardObjective.getScore(
                ChatColor.GREEN + "Balance:");
        score.setScore((int) (user.wallet.getBalance(0) / DENOMINATION_FACTOR));

        Score fuel = walletScoreboardObjective.getScore(
                ChatColor.GREEN + "Fuel:");
        Long feesBalance = 0L;
        try{
          feesBalance = user.wallet.getFuelBalance(true);
        } catch (Exception e) {
          System.out.println("[scoreboard] exception getting fees balance:\n" + e.toString());
        }
        fuel.setScore((int) (feesBalance / FEE_DENOMINATION_FACTOR));
        player.setScoreboard(walletScoreboard);

      } else {
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.runTaskAsynchronously(
            this,
            new Runnable() {
              @Override
              public void run() {
                try {
                  Score score =
                      walletScoreboardObjective.getScore(
                          ChatColor.GREEN + "Ems:"); // Get a fake offline player

                  score.setScore(user.countEmeralds(player.getInventory()));
                  player.setScoreboard(walletScoreboard);
                } catch (Exception e) {
                  System.out.println("problems in updatescoreboard");
                }
              }
            });
      } // end emerald here

    //check if wallet needs refil
    checkFuel(user.wallet);

    } catch (Exception e) {
      System.out.println("[scoreboard] rate limit");
    }
  }

  public void createPet(User user, String pet_name) {
    REDIS.sadd("pet:names", pet_name);
    BitQuest.REDIS.zincrby("player:tx", PET_PRICE, user.uuid.toString());
    long unixTime = System.currentTimeMillis() / 1000L;
    REDIS.set("pet:" + user.uuid.toString() + ":timestamp", Long.toString(unixTime));
    REDIS.set("pet:" + user.uuid.toString(), pet_name);
  }

  public void adoptPet(Player player, String pet_name) {
    try {
      final User user = new User(this.db_con, player.getUniqueId());
      if (user.wallet.getBalance(3) >= PET_PRICE) {
        try {
          if (user.wallet.payment(this.wallet.getAddress(), this.wallet.getPublicKey(), PET_PRICE) == true) {
            createPet(user, pet_name);
            spawnPet(player);
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else {
        player.sendMessage(
            ChatColor.RED
                + "You need "
                + PET_PRICE / DENOMINATION_FACTOR
                + " "
                + DENOMINATION_NAME
                + " to adopt a pet.");
      }
    } catch (Exception e) {
      e.printStackTrace();
      Bukkit.shutdown();
    }
  }

  public void spawnPet(Player player) {
    boolean cat_is_found = false;
    String cat_name = REDIS.get("pet:" + player.getUniqueId());
    for (World w : Bukkit.getWorlds()) {
      List<Entity> entities = w.getEntities();
      for (Entity entity : entities) {
        if (entity instanceof Ocelot) {
          if (entity.getCustomName() != null && entity.getCustomName().equals(cat_name)) {
            if (cat_is_found == false) {
              entity.teleport(player.getLocation());
              ((Ocelot) entity).setTamed(true);
              ((Ocelot) entity).setOwner(player);
              cat_is_found = true;
            } else {
              entity.remove();
            }
          }
        }
      }
    }
    if (cat_is_found == false) {
      final Ocelot ocelot =
          (Ocelot) player.getWorld().spawnEntity(player.getLocation(), EntityType.OCELOT);
      ocelot.setCustomName(cat_name);
      ocelot.setCustomNameVisible(true);
    }
    player.setMetadata("pet", new FixedMetadataValue(this, cat_name));
  }

  public void teleportToSpawn(Player player) {
    BitQuest bitQuest = this;
    // TODO: open the tps inventory
    player.sendMessage(ChatColor.GREEN + "Teleporting to spawn...");
    player.setMetadata("teleporting", new FixedMetadataValue(bitQuest, true));
    World world = Bukkit.getWorld("world");

    final Location spawn = world.getSpawnLocation();

    Chunk c = spawn.getChunk();
    if (!c.isLoaded()) {
      c.load();
    }
    bitQuest
        .getServer()
        .getScheduler()
        .scheduleSyncDelayedTask(
            bitQuest,
            new Runnable() {

              public void run() {
                player.teleport(spawn);
                if (REDIS.exists("pet:" + player.getUniqueId()) == true) {
                  spawnPet(player);
                }

                player.removeMetadata("teleporting", bitQuest);
              }
            },
            60L);
  }

  public void createScheduledTimers() {
    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

    //        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
    //            @Override
    //            public void run() {
    //                for (Player player : Bukkit.getServer().getOnlinePlayers()){
    //                    User user= null;
    //                    try {
    //                        // user.createScoreBoard();
    //                        updateScoreboard(player);
    //
    //                    } catch (ParseException e) {
    //                        e.printStackTrace();
    //                    } catch (org.json.simple.parser.ParseException e) {
    //                        e.printStackTrace();
    //                    } catch (IOException e) {
    //                        // TODO: Handle rate limiting
    //                    }
    //                }
    //            }
    //        }, 0, 120L);
    scheduler.scheduleSyncRepeatingTask(
        this,
        new Runnable() {
          @Override
          public void run() {
            // A villager is born
            World world = Bukkit.getWorld("world");
            world.spawnEntity(world.getSpawnLocation(), EntityType.VILLAGER);
          }
        },
        0,
        7200L);



    scheduler.scheduleSyncRepeatingTask(
        this,
        new Runnable() {
          @Override
          public void run() {
            run_season_events();
            publish_stats();
          }
        },
        0,
        30000L);
  }

  public void publish_stats() {
    try {
      Long balance = wallet.getBalance(0);
      REDIS.set("loot:pool", Long.toString(balance));
      if (System.getenv("ELASTICSEARCH_ENDPOINT") != null) {
        JSONParser parser = new JSONParser();

        final JSONObject jsonObject = new JSONObject();

        jsonObject.put("balance", balance);
        jsonObject.put("time", new Date().getTime());
        URL url = new URL(System.getenv("ELASTICSEARCH_ENDPOINT") + "-stats/_doc");
        System.out.println(url.toString());
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        con.setDoOutput(true);
        OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
        out.write(jsonObject.toString());
        out.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();
        System.out.println(response.toString());
        JSONObject response_object = (JSONObject) parser.parse(response.toString());
        System.out.println(response_object);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void run_season_events() {
    java.util.Date date = new Date();
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    int month = cal.get(Calendar.MONTH);
    if (month == 9) {
      World world = this.getServer().getWorld("world");
      world.setTime(20000);
      world.setStorm(false);
      spookyMode = true;
    } else {
      spookyMode = false;
    }
  }

  public void removeAllEntities() {
    World w = Bukkit.getWorld("world");
    List<Entity> entities = w.getEntities();
    int entitiesremoved = 0;
    for (Entity entity : entities) {
      entity.remove();
      entitiesremoved = entitiesremoved + 1;
    }
    System.out.println("Killed " + entitiesremoved + " entities");
  }

  public void killAllVillagers() {
    World w = Bukkit.getWorld("world");
    List<Entity> entities = w.getEntities();
    int villagerskilled = 0;
    for (Entity entity : entities) {
      if ((entity instanceof Villager)) {
        villagerskilled = villagerskilled + 1;
        ((Villager) entity).remove();
      }
    }
    for (Entity entity : entities) {
      if ((entity instanceof Giant)) {
        villagerskilled = villagerskilled + 1;
        ((Giant) entity).remove();
      }
    }
    w = Bukkit.getWorld("world_nether");
    entities = w.getEntities();
    for (Entity entity : entities) {
      if ((entity instanceof Villager)) {
        villagerskilled = villagerskilled + 1;
        ((Villager) entity).remove();
      }
      if ((entity instanceof Giant)) {
        villagerskilled = villagerskilled + 1;
        ((Giant) entity).remove();
      }
    }
    System.out.println("Killed " + villagerskilled + " villagers");
  }

  public void log(String msg) {
    Bukkit.getLogger().info(msg);
  }

  public int getLevel(int exp) {
    return (int) Math.floor(Math.sqrt(exp / (float) 256));
  }

  public int getExpForLevel(int level) {
    return (int) Math.pow(level, 2) * 256;
  }

  public float getExpProgress(int exp) {
    int level = getLevel(exp);
    int nextlevel = getExpForLevel(level + 1);
    int prevlevel = 0;
    if (level > 0) {
      prevlevel = getExpForLevel(level);
    }
    float progress = ((exp - prevlevel) / (float) (nextlevel - prevlevel));
    return progress;
  }

  public void setTotalExperience(Player player) {
    int rawxp = 0;
    if (BitQuest.REDIS.exists("experience.raw." + player.getUniqueId().toString())) {
      rawxp =
          Integer.parseInt(BitQuest.REDIS.get("experience.raw." + player.getUniqueId().toString()));
    }
    // lower factor, experience is easier to get. you can increase to get the opposite effect
    int level = getLevel(rawxp);
    float progress = getExpProgress(rawxp);
    player.setLevel(level);
    player.setExp(progress);
    setPlayerMaxHealth(player);
  }

  public void setPlayerMaxHealth(Player player) {
    // base health=6
    // level health max=
    int health = 8 + (player.getLevel() / 2);
    if (health > 40) health = 40;
    // player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE,
    // player.getLevel(), true));
    player.setMaxHealth(health);
  }

  public void saveLandData(Player player, String name, int x, int z)
      throws ParseException, org.json.simple.parser.ParseException, IOException {
    String chunk = "";
    if (player.getWorld().getName().equals("world")) {
      chunk = "chunk";
    } // end world lmao @bitcoinjake09
    else if (player.getWorld().getName().equals("world_nether")) {
      chunk = "netherchunk";
    } // end nether @bitcoinjake09
    BitQuest.REDIS.zincrby("player:tx", getLandPriceAtChunk(x, z), player.getUniqueId().toString());
    BitQuest.REDIS.set(chunk + "" + x + "," + z + "owner", player.getUniqueId().toString());
    BitQuest.REDIS.set(chunk + "" + x + "," + z + "name", name);
    land_owner_cache = new HashMap();
    land_name_cache = new HashMap();
    land_unclaimed_cache = new HashMap();
    player.sendMessage(
        ChatColor.GREEN
            + "Congratulations! You're now the owner of "
            + ChatColor.DARK_GREEN
            + name
            + ChatColor.GREEN
            + "!");
    updateScoreboard(player);
  }
  public boolean validName(final String name) {
    boolean hasNonAlpha = name.matches("^.*[^a-zA-Z0-9 _].*$");
    if(name.isEmpty()||name.length() > 28||hasNonAlpha||name.equalsIgnoreCase("the wilderness")) {
      return false;
    } else {
      return true;
    }
  }

  public Long getLandPriceAtChunk(int chunkX, int chunkZ) {
    //get spawn location
    World world = Bukkit.getWorld("world");
    final Location spawnLocation = world.getSpawnLocation();
    Chunk c = spawnLocation.getChunk();

    int x_spawn = c.getX();
    int z_spawn = c.getZ();

    //Chunk playerChunk = player.getLocation().getChunk();

    //int x = atChunk.getX();
    //int z = atChunk.getZ();

    //way easier distance check
    //int spawn_distance = (int) world.getSpawnLocation().distance();

    //pythagoreum therum
    //get distance from spawn in chunks

    double chunkxd = chunkX;
    double chunkzd = chunkZ;
    double spawnxd = x_spawn;
    double spawnzd = z_spawn;

    Double d = Math.sqrt(Math.pow(chunkxd - spawnxd, 2) + Math.pow(chunkzd - spawnzd, 2));
    System.out.println("[landprice] x:"+chunkxd+"z:"+chunkzd+"distance from spawn x:"+spawnxd+" z:"+spawnzd+" is: " + d.toString());

    //calculate price function
    //original:
    //Long spawnTax = Math.round(LAND_PRICE_SPAWN_TAX / (((d + 1) / LAND_PRICE_SPEED_FACTOR)));
    //Long finalPrice = LAND_PRICE + spawnTax;

    //sqrt version: price=(sqrt{-d+cutoff}*)+minimumPrice (cutoff=200, minimumprice=100)
    Long finalPrice;
    if(Math.round(d) >= LAND_TAX_DISTANCE || Math.round(d) < 0) {
      finalPrice = LAND_PRICE;
    } else {
      finalPrice = Math.round(Math.sqrt(-d+LAND_TAX_DISTANCE)*LAND_PRICE_SPAWN_TAX + LAND_PRICE);
    }
    
    System.out.println("[landprice] price is: " + finalPrice.toString() + " (" + LAND_PRICE);// + " + " + spawnTax.toString() + ")");

    return finalPrice;
  }

  public void claimLand(final String name, Chunk chunk, final Player player)
      throws ParseException, org.json.simple.parser.ParseException, IOException {
    String tempchunk = "";
    if (player.getLocation().getWorld().getName().equals("world")) {
      tempchunk = "chunk";
    } // end world lmao @bitcoinjake09
    else if (player.getLocation().getWorld().getName().equals("world_nether")) {
      tempchunk = "netherchunk";
    }
    // end nether @bitcoinjake09
    // check that land actually has a name
    final int x = chunk.getX();
    final int z = chunk.getZ();
    System.out.println(
        "[claim] "
            + player.getDisplayName()
            + " wants to claim "
            + x
            + ","
            + z
            + " with name "
            + name);
    if (REDIS.exists(tempchunk + "" + x + "," + z + "owner") == false) {

      if(validName(name)) {


            if (REDIS.get(tempchunk + "" + x + "," + z + "owner") == null) {
              try {

                final User user = new User(this.db_con, player.getUniqueId());
                player.sendMessage(ChatColor.YELLOW + "Claiming land...");
                BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
                final BitQuest bitQuest = this;

                  

                  if (user.wallet.payment(this.wallet.getAddress(), this.wallet.getPublicKey(), getLandPriceAtChunk(x, z))) {
                    saveLandData(player, name, x, z);
                  } else {

                    player.sendMessage(
                        ChatColor.RED + "Claim payment failed. Please try again later.");
                  }


              } catch (Exception e) {
                e.printStackTrace();
              }

            } else if (BitQuest.REDIS.get(tempchunk + "" + x + "," + z + "name").equals(name)) {
              player.sendMessage(ChatColor.DARK_RED + "You already own this land!");
            } else {
              // Rename land
              BitQuest.REDIS.set(tempchunk + "" + x + "," + z + "name", name);
              player.sendMessage(
                  ChatColor.GREEN
                      + "You renamed this land to "
                      + ChatColor.DARK_GREEN
                      + name
                      + ChatColor.GREEN
                      + ".");
            }

      } else {
        player.sendMessage(ChatColor.DARK_RED + "Invalid name.");
      }
    } else {
      player.sendMessage(ChatColor.DARK_RED + "This area is already claimed.");
    }
  }

  public boolean isOwner(Location location, Player player) {
    String chunk = "";
    if (player.getWorld().getName().equals("world")) {
      chunk = "chunk";
    } // end world lmao @bitcoinjake09
    else if (player.getWorld().getName().equals("world_nether")) {
      chunk = "netherchunk";
    } // end nether @bitcoinjake09
    String key =
        chunk + "" + location.getChunk().getX() + "," + location.getChunk().getZ() + "owner";
    if (REDIS.get(key).equals(player.getUniqueId().toString())) {
      // player is the owner of the chunk
      return true;
    } else {
      return false;
    }
  }

  public boolean canBuild(Location location, Player player) {
    // returns true if player has permission to build in location
    // TODO: Find out how are we gonna deal with clans and locations, and how/if they are gonna
    // share land resources
    String chunk = "";
    if (player.getWorld().getName().equals("world")) {
      chunk = "chunk";
    } 
    else if (player.getWorld().getName().equals("world_nether")) {
      chunk = "netherchunk";
    }
    if (!(location.getWorld().getName().equals("world"))
        && !(location.getWorld().getName().equals("world_nether"))) {
      // If theyre not in the overworld, they cant build
      return false;
    } else if (landIsClaimed(location)) {
      //check for sub-plots
      String owner_uuid =
            REDIS.get("chunk"
                    + location.getChunk().getX()
                    + ","
                    + location.getChunk().getZ()
                    + "owner");
      

      if (isOwner(location, player)) {
        return true;
      } else if (landPermissionCode(location).equals("p")) {//public
        return false;
      } else if(landPermissionCode(location).equals("pb")) {//public build permission.
        return true;
      } else if (landPermissionCode(location).equals("pv")) {//public pvp
        return true; 
      } else if (landPermissionCode(location).equals("v")) {//pvp
        return false; 
      } else if (landPermissionCode(location).equals("c")) {//clan
        String owner_clan = REDIS.get("clan:" + owner_uuid);
        String player_clan = REDIS.get("clan:" + player.getUniqueId().toString());
        if (owner_clan.equals(player_clan)) {
          return true;
        } else {
          return false;
        }
      }  else if (landPermissionCode(location).equals("ma")) {//mob arena
        if(isModerator(player)) {
          return true;
        } else {
          return false;
        }
      }  else if (landPermissionCode(location).equals("pma")) {//public mob arena
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  public Boolean canUse(Location location, Player player) {
    return true;
  }

  public static String landPermissionCode(Location location) {
    // permission codes:
    // p = public
    // c = clan
    // v = PvP(private cant build) by @bitcoinjake09
    // pv= public PvP(can build) by @bitcoinjake09
    // n = no permissions (private)
    String chunk = "";
    if (location.getWorld().getName().equals("world")) {
      chunk = "chunk";
    } // end world lmao @bitcoinjake09
    else if (location.getWorld().getName().equals("world_nether")) {
      chunk = "netherchunk";
    } // end nether @bitcoinjake09
    String key =
        chunk + "" + location.getChunk().getX() + "," + location.getChunk().getZ() + "permissions";
    if (land_permission_cache.containsKey(key)) {
      return land_permission_cache.get(key);
    } else if (REDIS.exists(key)) {
      String code = REDIS.get(key);
      land_permission_cache.put(key, code);
      return code;
    } else {
      return "n";
    }
  }

  public boolean createNewArea(Location location, Player owner, String name, int size) {
    // write the new area to REDIS
    JsonObject areaJSON = new JsonObject();
    areaJSON.addProperty("size", size);
    areaJSON.addProperty("owner", owner.getUniqueId().toString());
    areaJSON.addProperty("name", name);
    areaJSON.addProperty("x", location.getX());
    areaJSON.addProperty("z", location.getZ());
    areaJSON.addProperty("uuid", UUID.randomUUID().toString());
    REDIS.lpush("areas", areaJSON.toString());
    // TODO: Check if redis actually appended the area to list and return the success of the
    // operation
    return true;
  }

  public boolean isModerator(Player player) {
    if (REDIS.sismember("moderators", player.getUniqueId().toString())) {
      return true;
    } else if (ADMIN_UUID != null
        && player.getUniqueId().toString().equals(ADMIN_UUID.toString())) {
      return true;
    } else {
      return false;
    }
  }

  public void sendWalletInfo(final Player player, final User user) {
    if (BITCOIN_NODE_HOST != null) {
      // TODO: Rewrite send wallet info
    }
    try {
      Long balance = user.wallet.getBalance(0);
      Long displayBalance = balance / DENOMINATION_FACTOR;
      player.sendMessage("Address: " + user.wallet.getAddress());
      player.sendMessage("Balance: " + displayBalance.toString());
      player.sendMessage("-----------");

    } catch (Exception e) {
      e.printStackTrace();
      player.sendMessage(ChatColor.RED + "Error reading wallet. Please try again later.");
    }
  };

  public boolean landIsClaimed(Location location) {
    String chunk = "";
    if (location.getWorld().getName().equals("world")) {
      chunk = "chunk";
    } // end world lmao @bitcoinjake09
    else if (location.getWorld().getName().equals("world_nether")) {
      chunk = "netherchunk";
    } // end nether @bitcoinjake09
    String key =
        chunk + "" + location.getChunk().getX() + "," + location.getChunk().getZ() + "owner";

    if (REDIS.exists(key) == true) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    // we don't allow server commands (yet?)
    if (sender instanceof Player) {
      final Player player = (Player) sender;
      // PLAYER COMMANDS
      for (Map.Entry<String, CommandAction> entry : commands.entrySet()) {
        if (cmd.getName().equalsIgnoreCase(entry.getKey())) {
          entry.getValue().run(sender, cmd, label, args, player);
        }
      }

      // MODERATOR COMMANDS
      for (Map.Entry<String, CommandAction> entry : modCommands.entrySet()) {
        if (cmd.getName().equalsIgnoreCase(entry.getKey())) {
          if (isModerator(player)) {
            entry.getValue().run(sender, cmd, label, args, player);
          } else {
            sender.sendMessage(
                ChatColor.DARK_RED + "You don't have enough permissions to execute this command!");
          }
        }
      }
    }
    return true;
  }

  public boolean isPvP(Location location) {
    if ((landPermissionCode(location).equals("v") == true)
        || (landPermissionCode(location).equals("pv") == true))
    // if(SET_PvP.equals("true"))
    {
      return true;
    } // returns true. it is a pvp or public pvp and if SET_PvP is true

    return false; // not pvp
  }

  public boolean sendDiscordMessage(String content) {
    if (System.getenv("DISCORD_HOOK_URL") != null) {
      System.out.println("[discord] "+content);
      try {
        JSONParser parser = new JSONParser();

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", content);

        URL url = new URL(System.getenv("DISCORD_HOOK_URL"));
        HttpsURLConnection con = null;

        con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        con.setDoOutput(true);
        OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
        out.write(jsonObject.toString());
        out.close();
        // int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();
        System.out.println(response.toString());
        return true;
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }
    } else {
      return false;
    }
  }

  public void crashtest() {
    this.setEnabled(false);
  }
}
