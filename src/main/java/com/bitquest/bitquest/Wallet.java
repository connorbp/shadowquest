package com.bitquest.bitquest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;
import java.util.UUID;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.DatatypeConverter;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.ECKey.ECDSASignature;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileWriter;
import java.io.IOException;
//import org.json.simple.*;
import java.nio.charset.StandardCharsets;

public class Wallet {
  private String address;
  private String private_key;
  private String public_key;
  private String account;

  public Long lastRefilTime = 0L; // keeps track of when the wallet was refilled to avoid double spending on refils

  public Wallet(String _private_key, String _public_key, String _address, String _account) {
    this.public_key = _public_key;
    this.private_key = _private_key;
    this.address = _address;
    this.account = _account;
  }

  public String getAddress() {
    return this.address;
  }

  public String getPrivateKey() {
    return this.private_key;
  }

  public String getPublicKey() {
    return this.public_key;
  }

  public String getAccount() {
    return this.account;
  }

  public boolean payment(String _address, String _publicKey, Long gem) {
    System.out.println("[payment start] "+this.address+" -> "+gem + " -> " + _address);
    if(gem>1) {

      try{
        final String POST_URL = "http://" + BitQuest.SHADOW_NODE_HOST + ":" + BitQuest.SHADOW_NODE_PORT + "/nxt";
        final String POST_PARAMS = "requestType=transferCurrency&currency=" + BitQuest.CURRENCY_ID + "&recipient=" + _address + ((_publicKey != null) ? ("&recipientPublicKey=" + _publicKey) : "") + "&units=" + gem + "&secretPhrase=" + private_key + "&feeNQT=" + BitQuest.MINER_FEE +"&deadline=50";
        byte[] postData = POST_PARAMS.getBytes( StandardCharsets.UTF_8 );
        int postDataLength = postData.length;
        URL obj = new URL(POST_URL);
		    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		    con.setRequestMethod("POST");
		    con.setRequestProperty("User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
        con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
        con.setRequestProperty( "charset", "utf-8");
        con.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));

		    // For POST only - START
		    con.setDoOutput(true);
		    //OutputStreamWriter os = new OutputStreamWriter(con.getOutputStream());
        DataOutputStream wr = new DataOutputStream( con.getOutputStream());
        wr.write(postData);
		    wr.flush();
		    wr.close();
		    // For POST only - END

		    int responseCode = con.getResponseCode();
		    System.out.println("POST Response Code :: " + responseCode);

		    if (responseCode == HttpURLConnection.HTTP_OK) { //success
			    BufferedReader in = new BufferedReader(new InputStreamReader(
				    	con.getInputStream()));
			    String inputLine;
			    StringBuffer response = new StringBuffer();

			    while ((inputLine = in.readLine()) != null) {
			    	response.append(inputLine);
			    }
			    in.close();

			    // print result
          System.out.println("Received to Payment Request Response:");
			    System.out.println(response.toString());

          JSONParser parser = new JSONParser();
          JSONObject response_object = (JSONObject) parser.parse(response.toString());
          in.close();
          JSONObject transaction_object = (JSONObject) response_object.get("transactionJSON");
          JSONObject  attachment_object = (JSONObject) transaction_object.get("attachment");

          Double d = Double.parseDouble(attachment_object.get("units").toString().trim());

          if(d != null && d != 0) {
            //final Long sentBalance = d.longValue() / BitQuest.DENOMINATION_FACTOR;
            final Long sentBalance = d.longValue();
            System.out.println("Balance Value: " + (sentBalance / BitQuest.DENOMINATION_FACTOR));
            if(sentBalance != null && sentBalance != 0) {
              System.out.println("[payment success] " + this.address + " -> " + gem + " -> " + _address);
              return true;
            } else {
              System.out.println("[payment] transaction failed! No value sent.\n" + response_object.toString());
              return false;
            }
          } else {
            System.out.println("[payment] transaction failed! Returned value was null.\n" + response_object.toString());
            return false;
          }

		    } else {
		    	System.out.println("POST request failed (bad response code " + responseCode + ")");
          return false;
		    }
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }

    } else {
      return false;
    }

  }

  public Long importAddress(String account) throws IOException, ParseException {
    JSONParser parser = new JSONParser();
    final JSONObject jsonObject = new JSONObject();
    jsonObject.put("jsonrpc", "1.0");
    jsonObject.put("id", "bitquest");
    jsonObject.put("method", "importaddress");
    JSONArray params = new JSONArray();
    params.add(this.address);
    params.add(account);
    System.out.println("[importaddress] " + this.address+ " "+ account);
    jsonObject.put("params", params);
    URL url =
            new URL(
                    "http://"
                            + System.getenv("BITCOIND_PORT_8332_TCP_ADDR")
                            + ":"
                            + System.getenv("BITCOIND_PORT_8332_TCP_PORT"));
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setConnectTimeout(5000);
    String userPassword =
            System.getenv("BITCOIND_ENV_USERNAME") + ":" + System.getenv("BITCOIND_ENV_PASSWORD");
    String encoding = Base64.getEncoder().encodeToString(userPassword.getBytes());
    con.setRequestProperty("Authorization", "Basic " + encoding);

    con.setRequestMethod("POST");
    con.setRequestProperty(
            "User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    con.setDoOutput(true);
    OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
    out.write(jsonObject.toString());
    out.close();

    int responseCode = con.getResponseCode();

    BufferedReader in =
            new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    in.close();
    JSONObject response_object = (JSONObject) parser.parse(response.toString());
    System.out.println(response_object);
    return  Long.valueOf(0);
  }
  public Long getBalance(int confirmations) throws IOException, ParseException {
    
    //http://node1.shadowcrystals.xyz:8789/nxt?=/nxt&requestType=getBalance&account=SGE-PWPW-W9GA-65SZ-98KAW
    //http://node1.shadowcrystals.xyz:8789/nxt?requestType=getBalance&account=SGE-PWPW-W9GA-65SZ-98KAW
    try {
      JSONParser parser = new JSONParser();

      //URL url = new URL("http://" + BitQuest.SHADOW_NODE_HOST + ":" + BitQuest.SHADOW_NODE_PORT + "/nxt?requestType=getBalance&account=SGE-PWPW-W9GA-65SZ-98KAW");
      URL url = new URL("http://" + BitQuest.SHADOW_NODE_HOST + ":" + BitQuest.SHADOW_NODE_PORT + "/nxt?requestType=getAccountCurrencies&currency=" + BitQuest.CURRENCY_ID + "&account=" + address);
      System.out.println("Get URL: " + url.toString());
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setConnectTimeout(5000);

      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
      con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
      con.setRequestProperty( "charset", "utf-8");
      con.setRequestProperty( "Content-Length", Integer.toString( 0 ));
      
      int responseCode = con.getResponseCode();

      System.out.println("Received Response Code: " + responseCode);

      if (responseCode == HttpURLConnection.HTTP_OK) { //success
			    BufferedReader in = new BufferedReader(new InputStreamReader(
				    	con.getInputStream()));
			    String inputLine;
			    StringBuffer response = new StringBuffer();

			    while ((inputLine = in.readLine()) != null) {
			    	response.append(inputLine);
			    }
			    in.close();

          System.out.println("Received Response Data:\n" + response.toString());

          JSONObject response_object = (JSONObject) parser.parse(response.toString());

          if(!response_object.isEmpty()) {
            String units_string = response_object.get("units").toString();
            if(units_string.length() > 0) {
              Double d = Double.parseDouble(units_string.trim());
              //final Long balance = d.longValue() != 0 ? d.longValue() / BitQuest.DENOMINATION_FACTOR : 0;
              final Long balance = d.longValue();

              System.out.println("Got Shadow Balance from Node: " + balance);

              return balance;
            } else {
              System.out.println("Response String was empty. (No currency Held)");
              return Long.valueOf(0);
            }
          } else {
            System.out.println("GET request returned null json object");
            return Long.valueOf(0);
          }
      } else {
		    	System.out.println("GET request failed (bad response code " + responseCode + ")");
          return Long.valueOf(0);
		  }


    } catch(Exception e) {
      System.out.println("ran into an exception getting wallet balance");
      System.out.println(e);
      return Long.valueOf(0);
    }
    
  }

   public Long getFuelBalance(boolean confirmed) throws IOException, ParseException, Exception {
     try {
      JSONParser parser = new JSONParser();

      URL url = new URL("http://" + BitQuest.SHADOW_NODE_HOST + ":" + BitQuest.SHADOW_NODE_PORT + "/nxt?requestType=getBalance&account="+ address);
      System.out.println("Get URL: " + url.toString());
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setConnectTimeout(5000);

      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
      con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
      con.setRequestProperty( "charset", "utf-8");
      con.setRequestProperty( "Content-Length", Integer.toString( 0 ));
      
      int responseCode = con.getResponseCode();

      System.out.println("Received Response Code: " + responseCode);

      if (responseCode == HttpURLConnection.HTTP_OK) { //success
			    BufferedReader in = new BufferedReader(new InputStreamReader(
				    	con.getInputStream()));
			    String inputLine;
			    StringBuffer response = new StringBuffer();

			    while ((inputLine = in.readLine()) != null) {
			    	response.append(inputLine);
			    }
			    in.close();

          System.out.println("Received Response Data:\n" + response.toString());

          JSONObject response_object = (JSONObject) parser.parse(response.toString());

          if(!response_object.isEmpty()) {
            String units_string = confirmed ? response_object.get("balanceNQT").toString() : response_object.get("unconfirmedBalanceNQT").toString();
            if(units_string.length() > 0) {
              Double d = Double.parseDouble(units_string.trim());
              //final Long balance = d.longValue() != 0 ? d.longValue() / BitQuest.DENOMINATION_FACTOR : 0;
              final Long balance = d.longValue();

              System.out.println("Got ShadowGems Network Fee Balance from Node: " + balance);

              return balance;
            } else {
              throw new Exception("Response String was empty. (No currency Held)");
            }
          } else {
            throw new Exception("GET request returned null json object");
          }
      } else {
		    	throw new Exception("GET request failed (bad response code " + responseCode + ")");
		  }


     } catch (Exception e) {
       throw new Exception("ran into an exception getting network fees balance: " + e.toString());
     }
   }

  public boolean checkFuelCanSend() {
    try{
    if(getFuelBalance(true) > 0) {
      return true;
    } else {
      return false;
    }
    } catch (Exception e) {
      System.out.println("[fuelcansend] Error checking if user has enough fuel to send transactions.");
      return false;
    }
  }

   public boolean sendFuel(String _address, String _publicKey, Long fuel) {
    System.out.println("[payment start] "+this.address+" -> "+fuel + " -> " + _address);
    if(fuel>1) {

      try{
        final String POST_URL = "http://" + BitQuest.SHADOW_NODE_HOST + ":" + BitQuest.SHADOW_NODE_PORT + "/nxt";
        final String POST_PARAMS = "requestType=sendMoney&recipient=" + _address + ((_publicKey != null) ? ("&recipientPublicKey=" + _publicKey) : "") + "&amountNQT=" + fuel + "&secretPhrase=" + private_key + "&feeNQT=" + BitQuest.MINER_FEE + "&deadline=50";
        byte[] postData = POST_PARAMS.getBytes( StandardCharsets.UTF_8 );
        int postDataLength = postData.length;
        URL obj = new URL(POST_URL);
		    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		    con.setRequestMethod("POST");
		    con.setRequestProperty("User-Agent", "Mozilla/1.22 (compatible; MSIE 2.0; Windows 3.1)");
        con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
        con.setRequestProperty( "charset", "utf-8");
        con.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));

		    // For POST only - START
		    con.setDoOutput(true);
		    //OutputStreamWriter os = new OutputStreamWriter(con.getOutputStream());
        DataOutputStream wr = new DataOutputStream( con.getOutputStream());
        wr.write(postData);
		    wr.flush();
		    wr.close();
		    // For POST only - END

		    int responseCode = con.getResponseCode();
		    System.out.println("POST Response Code :: " + responseCode);

		    if (responseCode == HttpURLConnection.HTTP_OK) { //success
			    BufferedReader in = new BufferedReader(new InputStreamReader(
				    	con.getInputStream()));
			    String inputLine;
			    StringBuffer response = new StringBuffer();

			    while ((inputLine = in.readLine()) != null) {
			    	response.append(inputLine);
			    }
			    in.close();

			    // print result
          System.out.println("Received to Payment Request Response:");
			    System.out.println(response.toString());

          JSONParser parser = new JSONParser();
          JSONObject response_object = (JSONObject) parser.parse(response.toString());
          in.close();
          JSONObject transaction_object = (JSONObject) response_object.get("transactionJSON");

          Double d = Double.parseDouble(transaction_object.get("amountNQT").toString().trim());

          if(d != null && d != 0) {
            //final Long sentBalance = d.longValue() / BitQuest.DENOMINATION_FACTOR;
            final Long sentBalance = d.longValue();
            System.out.println("Balance Value: " + (sentBalance / BitQuest.DENOMINATION_FACTOR));
            if(sentBalance != null && sentBalance != 0) {
              System.out.println("[payment success] " + this.address + " -> " + fuel + " -> " + _address);
              return true;
            } else {
              System.out.println("[payment] transaction failed! No value sent.\n" + response_object.toString());
              return false;
            }
          } else {
            System.out.println("[payment] transaction failed! Returned value was null.\n" + response_object.toString());
            return false;
          }

		    } else {
		    	System.out.println("POST request failed (bad response code " + responseCode + ")");
          return false;
		    }
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }

    } else {
      return false;
    }

  }

  /*public String url() {//disabled unless i decide to add url for this currency
    if (address.substring(0, 1).equals("N")
        || address.substring(0, 1).equals("n")
        || address.substring(0, 1).equals("m")) {
      return "live.blockcypher.com/btc-testnet/address/" + address;
    }
    if (address.substring(0, 1).equals("D")) {
      return "live.blockcypher.com/doge/address/" + address;
    } else {
      return "live.blockcypher.com/btc/address/" + address;
    }
  }*/

  public boolean save(UUID uuid, Connection db_con) throws SQLException {
    PreparedStatement user_create_pst =
        db_con.prepareStatement(
            "INSERT INTO USERS (uuid,private,public,address,account) VALUES ('"
                + uuid.toString()
                + "','"
                + this.private_key
                + "','"
                + this.public_key
                + "','"
                + this.address
                + "','"
                + this.account
                + "')");
    user_create_pst.executeUpdate();

    return true;
  }
}
